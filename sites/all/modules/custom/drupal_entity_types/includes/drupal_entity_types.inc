<?php

/**
 * Page callback. Demos list.
 */
function drupal_entity_types_demos() {
  $links = array();
  $items = drupal_entity_types_menu();
  unset($items['demos']);
  foreach ($items as $url => $item) {
    $links[] = l($item['title'], $url) . ' : ' . $item['description'];
  }
  if (empty($links)) {
    $links[] = t('No demos yet.');
  }
  return theme('item_list', array('items' => $links));
}

function drupal_entity_types_demo_1() {

  // Load entity of entity_type node with ID 8

  $node = node_load(8);
  // dpm($node);
  // return 'Demo 1';

  // Load entity of entity_type comment with ID 5 and then get entity nid vid and bundle from entity object

  $id = 5;
  $entity_type = 'comment';
  $entity = entity_load($entity_type, array($id));
  $entity = reset($entity);

  // dpm($entity);
  // return 'Demo 1';

  $result = entity_extract_ids($entity_type, $entity);
  // dpm($result);
  // return 'Demo 1';

  // Load entity of entity_type comment, node and user with ID 5
  $output = array();
  // Given an $id and $type, load an entity.
  $id = 5;
  $types = array('node', 'user', 'comment');
  foreach ($types as $entity_type) {

    $entity = entity_load($entity_type, array($id));
    $entity = reset($entity);

    if ($entity) {

      $output[] = array(
        '#markup' => t('<p>The entity type is: %type</p>', array('%type' => $entity_type)),
      );

      list($id, $vid, $bundle) = entity_extract_ids($entity_type, $entity);

      $output[] = array(
        '#markup' => t('<p>ID: %id, Revision: %vid, Bundle: %bundle</p>', array('%id' => $id, '%vid' => $vid, '%bundle' => $bundle)),
      );

      $output[] = array(
        '#markup' => kpr($entity, TRUE),
      );
    }
  }
  return $output;
}

function drupal_entity_types_demo_2() {

  //public EntityFieldQuery::entityCondition($name, $value, $operator = NULL)

  $query = new EntityFieldQuery();
  $query->entityCondition('entity_type', 'node');
  $result = $query->execute();
  // dpm($result);
  // return "Demo 2";

  $query = new EntityFieldQuery();
  $query->entityCondition('entity_type', 'node');
  $query->entityCondition('bundle', 'page');
  $result = $query->execute();
  // dpm($result);
  // return "Demo 2";

  // public EntityFieldQuery::fieldCondition($field, $column = NULL, $value = NULL, $operator = NULL, $delta_group = NULL, $language_group = NULL)

  $query = new EntityFieldQuery();
  $query->fieldCondition('field_other_image', 'width', 200, '>');
  $result = $query->execute();
  // dpm($result);
  // return "Demo 2";

  // public EntityFieldQuery::propertyCondition($column, $value, $operator = NULL)

  $query = new EntityFieldQuery();
  $query->entityCondition('entity_type', 'node');
  $query->propertyCondition('promote', 1);
  $result = $query->execute();
  // dpm($result);
  // return "Demo 2";

  $query = new EntityFieldQuery();
  $query->entityCondition('entity_type', 'node');
  $query->propertyCondition('promote', 1);
  $query->fieldCondition('field_other_image', 'width', 200, '>');
  $query->fieldOrderBy('field_other_image', 'width', 'DESC');
  $result = $query->execute();
  // dpm($result);
  // return "Demo 2";

  $query = new EntityFieldQuery();
  $query->entityCondition('entity_type', 'node');
  $query->entityCondition('bundle', 'page');
  $query->range(0, 10);
  $result = $query->execute();
  dpm($result);
  $nids = array_keys($result['node']);
  dpm($nids);
  $nodes = entity_load('node', $nids);
  dpm($nodes);
  return "Demo 2";
}
